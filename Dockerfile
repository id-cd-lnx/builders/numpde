FROM fedora:33

LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"

RUN dnf update -y && dnf install -y which cmake gcc-c++

COPY entrypoint.sh /entrypoint.sh

RUN groupadd -g 1000 student && useradd -m -d /home/student -u 1000 -g 1000 student
COPY numpde /home/student
RUN mkdir /home/student/build

RUN cd /home/student/build && cmake .. && chown -R 1000:1000 /home/student

# Change ownership of mounted directory first, then `exec runuser -u student cmake ..`
ENTRYPOINT ["/bin/sh", "/entrypoint.sh"]
# CMD ["/bin/bash"]
