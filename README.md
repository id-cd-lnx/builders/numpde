Hunter/Cmake Environment for NumPDE
===

**Deactivate SELinux!**

Mount problems directory from the candidate filesystem to `/home/student/problems`.
I. e. from the git repo root on a Fedora system:

```
podman run --rm --volume $(pwd)/numpde/problems:/home/student/problems -i -t <image>
```

Podman Shared Image Storage
---------------------------

https://www.redhat.com/sysadmin/image-stores-podman
